import { featureConf } from "./config";
import  PostPages  from "./pages/PostPages";
import { Posts } from "./pages/Posts";

export const routes = [
  {
    key: `${featureConf}/catalog`,
    path: '/catalog',
    component: PostPages,
    exact: true,
  },
  {
    key: `${featureConf}/posts`,
    path: '/posts/:id',
    component: Posts,
    exact: true,
  },
];

