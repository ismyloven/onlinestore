import React, { useEffect, useState } from 'react';
import { useQuery } from "react-query";
import { getList } from "../api/PostApi";
import { Link as RouterLink } from 'react-router-dom';
import { Badge, Box, Button, Card, CardActionArea, CardContent, CardMedia, Grid, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Filters from "./Filters";
import { Rating } from "@material-ui/lab";
import debounce from 'debounce';
import { useDispatch } from "react-redux";
import { addCardToBasket } from "../../basket/actions";
import Categories from "./Categories";


const useStyles = makeStyles({
	margin: {
		marginTop: 50,
		padding: 20,
		height: 400,
	},
	marginRemove: {
		marginLeft: 20,

	},
});


function PostPages() {

	const classes = useStyles();
	const dispatch = useDispatch();

	const { data, error, isLoading } = useQuery('posts', async () => {
		let { data } = await getList();
		// console.log(data);
		return data;
	});


	const [filterTitle, setFilterTitle] = useState('');
	const [cashValue, setCashValue] = useState([20, 500]);
	const [rating, setRating] = useState([1, 50]);
	const [isNew, setIsNew] = useState(false);
	const [isSale, setIsSale] = useState(false);
	const [isInStock, setIsInStock] = useState(false);
	const [cats, setCats] = useState([]);
	const [filterQuery, setFilterQuery] = useState('');

	const setFilterQueryDebounced = debounce(setFilterQuery, 500);

	useEffect(() => {
		setFilterQueryDebounced(filterTitle);
	}, [filterTitle]);

	let array = data;


	const filterItems = () => {
		return array.filter((elem) => {
				let result = true;

				if (isNew) {
					result = result && elem.isNew;
				}

				if (isSale) {
					result = result && elem.isSale;
				}

				if (isInStock) {
					result = result && elem.isInStock;
				}

				if (filterQuery.length) {
					result = result && (elem.title.toLowerCase().includes(filterQuery.toLowerCase()));
				}

				if (cashValue) {
					let [min, max] = cashValue;
					result = result && (elem.price >= min && elem.price <= max);

				}

				if (rating) {
					let [min, max] = rating;
					result = result && (elem.rating >= min && elem.rating <= max);

				}

				if (cats.length > 0) {
					let newArray = result && (elem.categories.filter((item) => cats.includes(item)));
					if(newArray.length <1) {
						result = false;
					}
				}

				return result;
			}
		);
	};


	function addToBasket(item) {
		dispatch(addCardToBasket(item))
	}

	return (
		<Box>
			{isLoading ? (<Box>Loading...</Box>) : (error ? (<Box>Error: {error.message}</Box>)
				: (
					<Box>
						<Filters data={data} filterTitle={filterTitle} setFilterTitle={setFilterTitle}
								 cashValue={cashValue} setCashValue={setCashValue}
								 rating={rating} setRating={setRating}
								 isNew={isNew} setIsNew={setIsNew}
								 isSale={isSale} setIsSale={setIsSale}
								 isInStock={isInStock} setIsInStock={setIsInStock}
						/>
						<Categories selected={cats} setSelected={setCats}/>

						<Box>
							<Grid container item xs={12} spacing={3}>
								{filterItems().map((item) => (
									<Grid item xs={4} key={item.id}>
										<Card className={classes.margin}>
											<CardActionArea>
												<CardMedia
													component="img"
													alt="photo"
													height="140"
													image={item.photo}
													title="random_photo"
												/>
												<CardContent>
													<Box>
														{item.isNew ? <Badge badgeContent={"New"} color="primary">
															<Typography variant="h6">{item.title}</Typography>
														</Badge> : <Typography variant="h6">{item.title}</Typography>}
														<Typography variant="body2">{item.description}</Typography>
														<Box>
															{item.isSale ? <Badge badgeContent={"Sale"} color="primary">
																	<Typography variant="h6">{item.price} USD</Typography>
																</Badge> :
																<Typography variant="h6">{item.price} USD</Typography>}
															<Rating name="half-rating"
																	defaultValue={(item.rating) / 20}/>
															<Box>
															</Box>
															<Box>{item.isInStock}</Box>
															<Button component={RouterLink} to={`/posts/${item.id}`}>
																Get More Information
															</Button>
															<button className={classes.marginRemove} onClick={() => {
																addToBasket(item)
															}}>
																Add in Basket
															</button>
														</Box>
													</Box>
												</CardContent>
											</CardActionArea>
										</Card>
									</Grid>
								))}
							</Grid>
						</Box>
					</Box>))}
		</Box>
	);
}


export default PostPages;



