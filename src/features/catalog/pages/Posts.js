import React from 'react';
import { useParams } from "react-router";
import { useQuery } from "react-query";
import { get } from "../api/PostApi";
import Typography from "@material-ui/core/Typography";
import { Box, Button, Card, CardActionArea, CardContent, CardMedia, makeStyles } from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles({
	margin: {
		marginTop: 30,
		padding: 20,

	},

});


export function Posts() {
	const { id } = useParams();
	const { data, error, isLoading } = useQuery(["posts", id], async () => {
		let { data } = await get(id);
		return data;
	});

	const classes = useStyles();


	return (
		<Card className={classes.margin}>
			<CardActionArea>
				<CardContent>
					{isLoading ? (<Box>Loading...</Box>) : error ? (<Box>Error: {error.message}</Box>)
						: (<Box>
								<CardMedia
									component="img"
									alt="photo"
									height="340"
									image={data.photo}
									title="random_photo"
								/>
								<CardContent>
									<Box key={data.id}>
										<Typography variant="h4">{data.title}</Typography>
										<Typography variant="body1">{data.description}</Typography>
										<Box>
											<Box>{data.price} USD</Box>
											<Typography variant="h6">Rating of the product:</Typography>
											<Box><Rating name="half-rating"
														 defaultValue={(data.rating) / 20}/></Box>
										</Box>
										<Button component={RouterLink} to={`/catalog/`}>
											Back to the Catalog
										</Button>
									</Box>
								</CardContent>
							</Box>
						)}
				</CardContent>
			</CardActionArea>
		</Card>
	);
}

Posts.propTypes = {};