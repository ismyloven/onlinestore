import React, { useEffect } from 'react';
import { Box, Checkbox, makeStyles } from "@material-ui/core";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from "react-redux";
import {  sagaAddCategories } from "../../basket/actions";

const useStyles = makeStyles({
	categoriesStyle: {
		display: 'flex',
		justifyContent: 'space-between',
		flexWrap: 'wrap',
		padding: '10',
	}
});

function Categories({ selected, setSelected }) {
	const classes = useStyles();
	const dispatch = useDispatch();
	const  list  = useSelector((s) => s.categories.list);

	useEffect(() => {
			dispatch(sagaAddCategories());
	}, []);

	let handleChange = (id) => {
		let index = selected.indexOf(id);
		let newSelected = [];
		if (index >= 0) {
			newSelected = [...selected];
			newSelected.splice(index, 1)
		} else {
			newSelected = [...selected, id];
		}
		setSelected(newSelected);

	}
	return (
			<Box className={classes.categoriesStyle}>
			{list.map((item) =>
				(<div key={item.id}>{item.name}
					<Checkbox className={classes.categoriesStyle}
						checked={selected.includes(item.id)}
						onChange={() => handleChange(item.id)}
						inputProps={{ 'aria-label': 'primary checkbox' }}
					/>
				</div>))}
			</Box>
	);
}

export default Categories;

Categories.propTypes = {
	selected: PropTypes.arrayOf(PropTypes.string),
	setSelected: PropTypes.func,
}