import React from 'react';
import { Box, Slider, Switch } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: 50,
			width: '25ch',
		},
	},
	cashFilter: {
		width: 300,
	},
}));



function valuetext(value) {
	return `${value}`;
}

function Filters({ filterTitle, setFilterTitle,cashValue, setCashValue,rating, setRating,
					 isNew, setIsNew,isSale, setIsSale,isInStock, setIsInStock}) {
	const classes = useStyles();

	const onChange = (e) =>{
		setFilterTitle(e.target.value);
	}
	const handleChange = (event, newValue) => {
		setCashValue(newValue);

	};
	const handleFilterChange = (event, newValue) => {
		setRating(newValue);

	};
	return (
		<Box>
			<form className={classes.root} noValidate autoComplete="on">
				<TextField id="outlined-basic" label="Search by name" variant="outlined" value={filterTitle} onChange={onChange}/>

				<Box className={classes.cashFilter}>
					<Typography id="range-slider" gutterBottom>
						Filter by price
					</Typography>
					<Slider
						value={cashValue}
						onChange={handleChange}
						valueLabelDisplay="auto"
						aria-labelledby="range-slider"
						getAriaValueText={valuetext}
						min={0}
						max={1000}
					/>
				</Box>

				<Box className={classes.cashFilter}>
					<Typography id="range-slider" gutterBottom>
						Filter by rating
					</Typography>
					<Slider
						value={rating}
						onChange={handleFilterChange}
						valueLabelDisplay="auto"
						aria-labelledby="range-slider"
						getAriaValueText={valuetext}
						min={0}
						max={100}
					/>
				</Box>
				<Box>
					<Typography id="range-slider" gutterBottom>
						New products:
						<Switch
							checked={isNew}
							onChange={()=>setIsNew(!isNew)}
							name="isNew"
							inputProps={{ 'aria-label': 'secondary checkbox' }}
						/>
					</Typography>

					<Typography id="range-slider" gutterBottom>
						Filter for sale:
						<Switch
							checked={isSale}
							onChange={()=>setIsSale(!isSale)}
							name="isSale"
							inputProps={{ 'aria-label': 'secondary checkbox' }}
						/>
					</Typography>

					<Typography id="range-slider" gutterBottom>
						Filter "In stock":
						<Switch
							checked={isInStock}
							onChange={()=>setIsInStock(!isInStock)}
							name="isInStock"
							inputProps={{ 'aria-label': 'secondary checkbox' }}
						/>
					</Typography>

				</Box>

			</form>


		</Box>
	);
}

export default Filters;