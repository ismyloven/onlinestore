import React from 'react';
import { Box, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";



const useStyles = makeStyles({
	margin: {
		display: 'flex',
		justifyContent: 'center',
		marginTop: 50,
		padding: 20,
	},
	display: {
		display: 'flex',
	},


});



function HomePages() {
	const classes = useStyles();
	return (
		<Box>

			<Typography variant="h3" className={classes.margin}>Building an online store</Typography>
			<Box className={classes.display}>
			<Box>
			<Typography variant="h5">Make your first sale today!</Typography>

			Yes, it’s that fast. Shopify provides you with everything you need to start accepting orders the very same day you sign up.
				A good business owner must understand their online store. Gain incredible insight into your performance through Shopify’s built-in analytics features.

				We help you easily track your progress over time so you can make better business decisions in the long run.
			Simply choose a theme, customize your site, add products and begin processing customers’ credit cards right away.
				</Box>
			<Box>
			<Typography variant="h5">Loads of functionality</Typography>

			Browse through Shopify’s wide selection of apps to add practical features to your website quickly and easily.
				With the Shopify and Advanced Shopify plans, you can take advantage of Shopify’s coupon engine to promote your business through attractive discount codes and coupons.

				Offer attractive savings to your customers by dollar amount, percentage amount or even through shipping deals
			Our apps can help you improve your marketing abilities, seamlessly manage your online store, enhance SEO and understand consumer buying behaviours — just to name a few.
				</Box>
			<Box>
			<Typography variant="h5">Insight into your customers</Typography>
				We want your customers to find you on the web, which is why we support search engine optimization best practices including H1 tags, title tags and meta tags.

				To make sure newly added products and pages are reflected in the search results, Shopify automatically generates sitemap.xml files for your online store.
			Who is purchasing from you? What are they buying? How can you improve your ecommerce store to reflect their habits? Shopify’s admin area gives you the information you need to make great business decisions.
			</Box>

			</Box>
		</Box>
	);
}

export default HomePages;