import HomePages  from './pages/HomePages';
import { featureConf } from "./config";

export const routes = [
  {
    key: `${featureConf}/home-page`,
    path: '/',
    component: HomePages,
    exact: true,
  },
];
