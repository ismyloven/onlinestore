import React from 'react';
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
    margin: {
        marginTop: 50,
        padding: 20,
    },
});


export function DeliveryService() {

    const classes = useStyles();

    return (
        <Container className={classes.margin} maxWidth="md">
            <Typography align="center" variant="h4" gutterBottom>
                Hello! Marhaba! Aloha! Shalom! Guten Tag! Привет!
            </Typography>
            <Typography component="p" style={{ textIndent: '1.5rem' }}>
                We are a team of developers and testers from one multifunctional unit - Me. This application will help you find
                a movie of interest by title, sort the result and get detailed information about the movie.
            </Typography>
        </Container>
    );
}


export default DeliveryService;