import { DeliveryService } from './pages/DeliveryService';
import { featureConf } from "./config";

export const routes = [
  {
    key: `${featureConf}/delivery`,
    path: '/delivery',
    component: DeliveryService,
    exact: true,
  },
];
