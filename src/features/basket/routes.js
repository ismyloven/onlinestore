import { Basket } from './pages/Basket';
import { featureConf } from "./config";
import OrderPage from "./pages/OrderPage";
import OrderPageStepTwo from "./pages/OrderPageStepTwo";
import OrderPageStepThree from "./pages/OrderPageStepThree";

export const routes = [
  {
    key: `${featureConf}/basket`,
    path: '/basket',
    component: Basket,
    exact: true,
  },
  {
    key: `${featureConf}/orders`,
    path: '/orders',
    component: OrderPage,
    exact: true,
  },
  {
    key: `${featureConf}/step2`,
    path: '/step2',
    component: OrderPageStepTwo,
    exact: true,
  },
  {
    key: `${featureConf}/step3`,
    path: '/step3',
    component: OrderPageStepThree,
    exact: true,
  },
];
