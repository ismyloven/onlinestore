import React from 'react';
import { useSelector } from "react-redux";
import { Box, Button, makeStyles } from "@material-ui/core";
import { Link as RouterLink } from "react-router-dom";
import ProductPosts from "./ProductPosts";
import Typography from "@material-ui/core/Typography";
import axios from "../../../services/axios";
import "./order.css";


const useStyles = makeStyles({
	margin: {
		marginTop: 50,
		padding: 20,
		fontSize: 20,
		width: 1240,
	},


	center: {
		marginTop: 20,
		display: 'flex',
		justifyContent: 'center',
		flexDirection: 'column,'
	},
	button: {
		marginTop: 50,
		marginBottom: 60,
	},
	add: {
		textAlign: 'center',
		fontSize: 26,
		marginTop: 50,

	},
	orderButton: {
		display: 'flex',
		justifyContent: 'space-around',
	}
});

function OrderPageStepTwo() {
	const classes = useStyles();
	const { store: { data } } = useSelector((s) => s);
	const { store: { orders } } = useSelector((s) => s);
	let cost = (data.length < 1 ? 0 : data.map((item) => item.price).reduce((a, b) => +a + (+b)));


	let onPostSubmit = () => {
		axios.post('https://60e4720a5bcbca001749e9e1.mockapi.io/order',
			JSON.stringify(orders)
		)
			.then((response) => {
				console.log(response);
			})
			.catch((error) => {
				console.log(error);
			});
	}

	return (
		<Box>
			<ProductPosts/>
			{orders.firstName.length < 1 ? <Typography className={classes.add} variant="h4">
				Please, enter your data
			</Typography> : <Box>
				<Box className='order'>
					<Box>Name:{orders.firstName}</Box>
					<Box>Second name:{orders.lastName}</Box>
					<Box>Country:{orders.country}</Box>
					<Box>Phone:{orders.phone}</Box>
					<Box>City:{orders.city}</Box>
					<Box>Address:{orders.address}</Box>
					<Box>Second address:{orders.address2}</Box>
					<Box>Email:{orders.email}</Box>
					<Box>Delivery type:{orders.deliveryType}</Box>
					<Box>Dont call me:{orders.dontCallMe}</Box>
					<Box>Your comment:{orders.comment}</Box>
					<Box>
						<Typography className={classes.center} variant="h4">Total Price is: {cost} USD </Typography>
					</Box>
				</Box>
			</Box>}
			<Box className={classes.orderButton}>
				<Button className={classes.button} component={RouterLink} to={`/orders/`} >
					Edit
				</Button>
				{cost === 0 ? null :
					<Button className={classes.button} component={RouterLink} to={`/step3/`} onClick={onPostSubmit} >
						Make an Order
					</Button>}
			</Box>
		</Box>
	);
}

export default OrderPageStepTwo;