import React from 'react';
import { Link as RouterLink} from "react-router-dom";
import { Box, Button, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useSelector } from "react-redux";


const useStyles = makeStyles({
	margin: {
		marginTop: 50,
		padding: 20,
		fontSize: 30,

	},
	button: {
		display:'flex',
		justifyContent: 'center',
		marginTop: 50,
		padding: 20,
		fontSize: 20,
	}
});

function OrderPageStepThree() {
	const { store: { orders } } = useSelector((s) => s);
	const classes = useStyles();
	return (
		<Box>
			<Box>
				<Typography className={classes.margin}>
					Your order has been completed with id:{orders.id} . Our manager will contact you shortly.
					Thank you!
				</Typography>
			</Box>
			<Button className={classes.button} component={RouterLink} to={`/catalog/`} >
				Catalog of the products
			</Button>

		</Box>
	);
}

export default OrderPageStepThree;