import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { Box, Button, makeStyles } from "@material-ui/core";
import { OrderFromBasket } from "../actions";
import { Link as RouterLink } from "react-router-dom";
import "./order.css";



const useStyles = makeStyles({
	margin: {
		marginTop: 50,
		display: "flex",
		justifyContent: "center",
	},
	flex: {
		marginTop: 50,
		display: "flex",
		flexDirection: "column",
	},

});

const countries = ['Ukraine', 'Poland', 'Israel', 'Germany', 'Austria', 'Turkey', 'Russia', 'China'];

const deliveryTypes = ['Pickup','Postal Service'];

function OrderPage() {
	const classes = useStyles();
	const dispatch = useDispatch();

	const [server, setServer] = useState({
		firstName: '',
		lastName: '',
		country: [],
		phone: '',
		city: '',
		address: '',
		address2: '',
		email: '',
		deliveryType: 'Postal Service',
		dontCallMe: false,
		comment: '',
		id: new Date().getTime(),
	});


	let handleChangeName = (e) => {
		let code = e.target.value;
		code = code.replace(/\d/gmi, '');
		setServer({ ...server, firstName:code });

	}
	let handleChangeSecondName = (e) => {
		let surname = e.target.value;
		surname = surname.replace(/\d/gmi, '');
		setServer({ ...server, lastName: surname });

	}
	let handleSelectCountry = (e) => {
		setServer({ ...server, country: e.target.value });

	}
	let handleChangePhone = (e) => {
        let phoneValue = e.target.value;
		phoneValue = phoneValue.replace(/\D/gmi, '');
        setServer({ ...server, phone: phoneValue });
	}

	let handleChangeCity = (e) => {
		let city = e.target.value;
		city = city.replace(/\d/gmi, '');
		setServer({ ...server, city: city });

	}
	let handleChangeAddress = (e) => {
		setServer({ ...server, address: e.target.value });

	}
	let handleChangeAddress2 = (e) => {
		setServer({ ...server, address2: e.target.value });

	}
	let handleChangeEmail = (e) => {
		let email = e.target.value;
		setServer({ ...server, email: email});
	}

	function checkValidMail() {
		let re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
		let myMail = server.email;
		let valid = re.test(myMail);
		if (!valid) {
			alert('Please enter correct email address');
		}
		return valid;
	}



	let handleDeliveryTypes = (e) => {
		setServer({ ...server, deliveryType: e.target.value });

	}

	let handleCheckBox = () => {
		setServer({ ...server, dontCallMe: !server.dontCallMe });

	}

   let handleComment = (e) => {
	setServer({ ...server, comment: e.target.value });

}

	let handleSubmit = (e) => {
		e.preventDefault();
		checkValidMail();
		let submitProduct = server;
		 let ser = JSON.stringify(submitProduct);
		 console.log(ser);
	};


	function addOrderStore(server) {
		dispatch(OrderFromBasket(server));
	}

	return (
		<Box className='container'>
			<Typography className={classes.margin} variant="h5">"Order - Step 1"</Typography>

			<form className={classes.flex} onSubmit={handleSubmit}>
				<label> Enter your name:
				<input type="text" value={server.firstName} onChange={handleChangeName} required/>
				</label>
				<label> Enter your surname:
				<input type="text" value={server.lastName} onChange={handleChangeSecondName} required/>
			</label>
				<label>Country : <select  value={server.country} onChange={handleSelectCountry} required>
					{countries.map(countryLabel => (
						<option key={countryLabel} value={countryLabel}>{countryLabel}</option>
					))}
				</select>
				</label>
				<label> Enter your phone:
					<input type="text" value={server.phone} onChange={handleChangePhone} required/>
				</label>
				<label> Enter your city:
					<input type="text" value={server.city} onChange={handleChangeCity} required/>
				</label>
				<label> Enter your address:
					<input type="text" value={server.address} onChange={handleChangeAddress} required/>
				</label>
				<label> Enter your second address:
					<input type="text" value={server.address2} onChange={handleChangeAddress2}/>
				</label>
				<label> Enter your Email:
					<input type="text" value={server.email} onChange={handleChangeEmail} required/>
				</label>
				<Box>
					{deliveryTypes.map((prod)=>( <label key={prod}>
						<Box>{prod} <input type='radio' value={prod} name='delivery'
										   checked={server.deliveryType === prod}
										   onChange={handleDeliveryTypes}/></Box>
					</label> ))}
				</Box>

				<label>Dont Call Me:
					<input type='checkbox' name='action'
						   checked={server.dontCallMe}
						   onChange={handleCheckBox}/>
				</label>

				<label>Leave your comment:
					<textarea value={server.comment} onChange={handleComment} maxLength={"500"}/>
				</label>
<Box className='box'>
	<Button className="submit" component={RouterLink} to={`/step2/`} onClick={() =>{addOrderStore(server)}}>
		Send
	</Button>
	<Button className="button" component={RouterLink} to={`/basket/`}>
		Cancel
	</Button>
</Box>
			</form>
		</Box>
	);
}

export default OrderPage;