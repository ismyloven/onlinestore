import React from 'react';
import ProductPosts from "./ProductPosts";
import { Link as RouterLink} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import { useSelector } from "react-redux";
import { Box, Button, makeStyles } from "@material-ui/core";


const useStyles = makeStyles({
    margin: {
        marginTop: 50,
        padding: 20,
        fontSize: 20,
        width: 1240,
    },
    add: {
        textAlign:'center',
        fontSize: 20,
        marginTop: 50,

    }
});


export function Basket() {
    const classes = useStyles();
    const { store: { data } } = useSelector((s) => s);
    let cost = (data.length < 1 ? 0 : data.map((item) => item.price).reduce((a, b) => +a + (+b)))



    return (
        <div className="page">

            <ProductPosts/>
            {data.length <1
                ? <Typography className={classes.add} variant="h5">Add products to your Basket from
                    <Button  component={RouterLink} to={`/catalog/`}>
                        Catalog of the products
                    </Button></Typography>
                : <Box>
                <Box>
                <Typography className={classes.margin} variant="h5">Total: {data.length} products in your
                    Basket</Typography>
            </Box>
                <Box>
                <Typography className={classes.margin} variant="h5">Total Price is:
            {cost} USD
                </Typography>
                </Box>
                <Box>
                    <Button className={classes.margin} component={RouterLink} to={`/orders/`}>
                        Make an order
                    </Button>

                </Box>
                </Box>
            }

        </div>
    );
}


