import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import {
	Box, CardMedia,
	makeStyles,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow
} from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import { removeCardFromBasket } from "../actions";
import DeleteIcon from '@material-ui/icons/DeleteForever';





const useStyles = makeStyles({
	margin: {
		marginTop: 50,
		padding: 20,
		height: 400,
	},
	marginRemove: {
		marginLeft: 20,

	},
	icon: {
		marginLeft: 110,

	},
	table: {
		minWidth: 700,
	},
	tableMargin: {
		marginTop: 50,
	}
});

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: theme.palette.common.main,
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);


function ProductPosts() {

	const classes = useStyles();
	const { store: { data } } = useSelector((s) => s);
	const dispatch = useDispatch();
	// console.log(data);

	const keys = ["rating", "categories", "photo", "price", "description", "title", "createdAt", "id"];
	let counterObj = {};
	let keyForCounterObj;
	data.forEach((obj) => {
		keyForCounterObj = '';
		keys.forEach((key) => {
			keyForCounterObj += String(obj[key])
		});
		if (counterObj[keyForCounterObj]) {
			counterObj[keyForCounterObj].times++
		} else {
			counterObj[keyForCounterObj] = {
				...obj,
				times: 1
			}
		}
	})

	let newArrayOfObjects = [];
	const counterObjKeys = Object.keys(counterObj)
	counterObjKeys.forEach((key) => {
		newArrayOfObjects.push(counterObj[key])
	})


	function onDelete(id) {
		dispatch(removeCardFromBasket(id));
	}

	return (
		<Box>
		{newArrayOfObjects.length >0 ?<TableContainer component={Paper} className={classes.tableMargin}>
		<Table className={classes.table} aria-label="customized table">
			<TableHead>
				<TableRow>
					<StyledTableCell>Photo of the product</StyledTableCell>
					<StyledTableCell align="right">Name</StyledTableCell>
					<StyledTableCell align="right">Price&nbsp;(USD)</StyledTableCell>
					<StyledTableCell align="right">Count&nbsp;</StyledTableCell>
					<StyledTableCell align="right">Total sum&nbsp;(USD)</StyledTableCell>
					<StyledTableCell align="right">Remove</StyledTableCell>
				</TableRow>
			</TableHead>
			<TableBody>
				{newArrayOfObjects.map((row) => (
					<StyledTableRow key={row.id}>
						<StyledTableCell><CardMedia
							component="img"
							alt="photo"
							height="50"
							width="50"
							image={row.photo}
							title="random_photo"
						/></StyledTableCell>
						<StyledTableCell align="right">{row.title}</StyledTableCell>
						<StyledTableCell align="right">
							{row.price}
						</StyledTableCell>

						<StyledTableCell align="right">{row.times}</StyledTableCell>

						<StyledTableCell align="right">{row.price * row.times}</StyledTableCell>
						<StyledTableCell align="right">

							<StyledTableCell align="right">
								<DeleteIcon color="primary" onClick={() => onDelete(row.id)} />
							</StyledTableCell>

						</StyledTableCell>
					</StyledTableRow>
				))}
			</TableBody>
		</Table>
	</TableContainer> : null}
		</Box>

	);

}

export default ProductPosts;