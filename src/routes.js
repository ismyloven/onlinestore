import { routes as about } from "./features/about/routes";
import { routes as catalog } from "./features/catalog/routes";
import{routes as delivery} from "./features/delivery/routes";
import{routes as basket} from "./features/basket/routes";


export const routes = [
  // put here features' routes
  ...about,
  ...catalog,
    ...delivery,
    ...basket,
];
