import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import purple from '@material-ui/core/colors/purple';



export const theme = createMuiTheme({
	spacing: 5,
	palette: {
		primary: {
			main: blue[300],
		},
		secondary: {
			main: purple[900],
		},
		common:{
			main: purple[100]
		}
	},
});