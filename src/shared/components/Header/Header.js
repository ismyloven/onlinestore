import React, { Fragment } from 'react';
import { AppBar, Badge, Box, Button, IconButton, makeStyles, Toolbar } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { NavLink } from "react-router-dom";
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { withStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";


const useStyles = makeStyles((theme) => ({
	offset: theme.mixins.toolbar,
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},

	size: {
		fontSize: 40,
	},
}));

const StyledBadge = withStyles((theme) => ({
	badge: {
		right: -3,
		top: 13,
		border: `2px solid ${theme.palette.background.paper}`,
		padding: '0 4px',
	},
}))(Badge);

function Header(props) {
	const classes = useStyles();
	const { store: { data } } = useSelector((s) => s);
	return (
		<Fragment>
			<div className={classes.offset}/>
			<AppBar position="fixed">
				<Toolbar>
					<IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
<ShoppingCartOutlinedIcon className={classes.size}/>
					</IconButton>
					<Typography variant="h4" className={classes.title}>
						Store
					</Typography>
					<Box>
						<Button color="inherit" to="/" exact component={NavLink}>About us</Button>
						<Button color="inherit" to="/catalog" exact component={NavLink}>Catalog</Button>
						<Button color="inherit" to="/delivery" exact component={NavLink}>Delivery</Button>
						<Button color="inherit" to="/basket" exact component={NavLink}>Basket
							{data.length<1 ? null : <IconButton aria-label="cart">
								<StyledBadge badgeContent={data.length} color="secondary">
									<ShoppingCartIcon/>
								</StyledBadge>
							</IconButton>}
						</Button>
					</Box>
				</Toolbar>
			</AppBar>
		</Fragment>
	);
}

export default Header;