import React from 'react';
import { Box, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
    margin: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        fontSize: 20,

    }
});

export function Page404() {
    const classes = useStyles();
  return (
    <Box className="page404"  className={classes.margin}>
      <Box>
          <Typography variant="h3"> 404 Error </Typography>
          <Typography variant="h3"> PAGE NOT FOUND </Typography>
     <Typography variant="h3"> BUT NEVER STOP TRYING </Typography>
      </Box>
    </Box>



  );
}

Page404.propTypes = {};
